import requests
from typing import Dict, List
from pprint import pprint

BASE_URI = "https://api.guildwars2.com/v2/"

def get_daily_missions() -> Dict[str, str]:
    req = requests.get(BASE_URI + "achievements/daily")
    return req.json()

def get_achievements(*ids:List[str]) -> Dict[str, str]:
    """
    param ids List[str]: list of ids
    """
    str_ids = ",".join(ids)
    req = requests.get(BASE_URI + f"achievements?ids={str_ids}")
    return req.json()

def get_commerce(header : Dict[str, str]) -> Dict[str, str]:
    req = requests.get(BASE_URI + "commerce/delivery", headers=header)
    return req

class FailedRequestError(Exception):
    def __init__(self, statuscode, message="Request failed"):
        self.statuscode = statuscode
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f"{self.message} (status {self.statuscode})"
