from . import api
from pprint import pprint
from typing import Dict, List

_dailies = api.get_daily_missions()

def _parse(dailies: Dict[str, str]):
    ids = []
    for daily in dailies:
        ids.append(str(daily["id"]))

    return ids

def get_dailies(mission_type:str):
    """
    mission_type can be fractal, pve, pvp, wvw, special
    """
    frac = _parse(_dailies[mission_type])
    missions = api.get_achievements(*frac)
    data = []

    for mission in missions:
        data.append({"type": mission_type,
                     "name": mission["name"],
                     "description": mission["description"],
                     "requirement": mission["requirement"]})
    return data

def get_all_dailies():
    pve = get_dailies("pve")
    pvp = get_dailies("pvp")
    wvw = get_dailies("wvw")
    frac = get_dailies("fractals")

    pve += frac + pvp + wvw
    return pve

class Daily:
    def __init__(self, daily_id):
        self.daily_id = daily_id

    def get_name(self):
        pass

    def get_description(self):
        pass

    def get_requirements(self):
        pass
