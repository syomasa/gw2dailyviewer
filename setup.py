from setuptools import setup

setup(
    name="gw2ApiBot",
    version="1.0",
    description="Shows dailies for guild wars 2",
    author="syomasa",
    packages=["gw2"],
    install_requires=["flask", "simplejson", "python-dotenv", "requests"]
)

