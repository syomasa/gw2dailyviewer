const baseUrl = "";
function getData(request_ending) {
	const promise = fetch(baseUrl + request_ending);
	return promise.then(data => {return data});
};

function addListElements(index, content, className) {
	const elements = document.getElementsByClassName("daily");
	const newDiv = document.createElement("div");
	newDiv.className = className;
	newDiv.innerHTML = content;

	elements[index].appendChild(newDiv);
	return newDiv;
};

function makeHoverElement(content, parent) {
	const newDiv = document.createElement("div");

	newDiv.className = "hoverElementHidden";
	newDiv.innerHTML = content;
	parent.appendChild(newDiv);
	return newDiv;

};

function createDiv(content, parent) {
	const newDiv = document.createElement("div");
	newDiv.innerHTML = content;
	parent.appendChild(newDiv);
	return newDiv;
}

function mouseOver(parent, child) {
	parent.onmouseover = () => {
		var t;
		hideAll();
		clearTimeout(t);
		child.className = "hoverElement";
	};
	parent.onmouseout = () => {
		t = setTimeout(() => {
			child.className = "hoverElementHidden";
		}, 300);
	};
};

function hideAll() {
	const elements = document.getElementsByClassName("hoverElement");
	for (let i = 0; i < elements.length; i++) {
		elements[i].className = "hoverElementHidden";
	};
};

function submitHandler(api_key) {
	return axios.get("/api/marketplace", {
		params: {
			Authorization: `Bearer ${api_key}`
		}
	}).then(response => {return response});
};

function inputHandler(event) {
	event.preventDefault();
	let input = document.getElementById("apiKey");
	let div = document.getElementById("market");
	submitHandler(input.value)
		.then(res => {
			input.value = "";

			div.id = "deliveries";
			div.innerHTML = ""; // reset

			let currencies = createDiv("", div);
			currencies.id = "currencies";

			let currency = createDiv("Gold: " + res.data.gold, currencies);
			currency.className = "currency";

			currency = createDiv("Silver: " + res.data.silver, currencies);
			currency.className = "currency";

			currency = createDiv("Copper:" + res.data.copper, currencies);
			currency.className = "currency";
		})
		.catch(err => {
			errorDiv = document.getElementsByClassName("errorMsg")[0];
			errorDiv.dataset.error = true;
			errorDiv.innerHTML = "Invalid api key";
			setTimeout(() => {
				errorDiv.dataset.error = false;
			}, 3000)
		})
	input.value = "";
}

const form = document.getElementById("inputForm");
form.addEventListener("submit", inputHandler);


getData("api/dailies/pve")
	.then(response => response.json())
	.then(html => {
		addListElements(0, "<h3>Open world</h3>", "pve")
		for(let i = 0; i < html.length; i++) {
			let newElement = addListElements(0, html[i]["name"], "pve");
			let childElement = makeHoverElement(html[i]["requirement"], newElement);
			mouseOver(newElement, childElement);
		}
	});

getData("api/dailies/fractals")
	.then(response => response.json())
	.then(html => {
		addListElements(1, "<h3>Fractals</h3>", "fractals")
		for(let i = 0; i < html.length; i++) {
			let newElement = addListElements(1, html[i]["name"], "fractals");
			let childElement = makeHoverElement(html[i]["requirement"], newElement);
			mouseOver(newElement, childElement);
		}
	});

getData("api/dailies/pvp")
	.then(response => response.json())
	.then(html => {
		addListElements(2, "<h3>Player vs Player</h3>", "pvp")
		for(let i = 0; i < html.length; i++) {
			let newElement = addListElements(2, html[i]["name"], "pvp");
			let childElement = makeHoverElement(html[i]["requirement"], newElement);
			mouseOver(newElement, childElement);
		}
	});

getData("api/dailies/wvw")
	.then(response => response.json())
	.then(html => {
		addListElements(3, "<h3>World vs World</h3>", "wvw")
		for(let i = 0; i < html.length; i++) {
			let newElement = addListElements(3, html[i]["name"], "wvw");
			let childElement = makeHoverElement(html[i]["requirement"], newElement);
			mouseOver(newElement, childElement);
		}
	});
