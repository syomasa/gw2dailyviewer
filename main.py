import settings
import gw2
import json
import flask
from pprint import pprint
from flask import Flask, request

app = Flask(__name__, static_folder='static')

@app.route("/")
def hello():
    return flask.current_app.send_static_file("index.html")

@app.route("/api/dailies")
def dailies():
    return flask.jsonify(gw2.dailies.get_all_dailies())

@app.route("/api/dailies/pve")
def pve():
    return flask.jsonify(gw2.dailies.get_dailies("pve"))

@app.route("/api/dailies/pvp")
def pvp():
    return flask.jsonify(gw2.dailies.get_dailies("pvp"))

@app.route("/api/dailies/wvw")
def wvw():
    return flask.jsonify(gw2.dailies.get_dailies("wvw"))

@app.route("/api/dailies/fractals")
def fractals():
    return flask.jsonify(gw2.dailies.get_dailies("fractals"))

@app.route("/api/marketplace")
def market():
    header = request.args
    resp = gw2.api.get_commerce(header.to_dict())
    resp_data = resp.json()
    if(resp.status_code == 200):
        coins = resp_data["coins"]
        currencies = []
        dividier = 10000
        while(dividier >= 1):
            currency = str(int(coins / dividier))
            currencies.append(currency)
            coins -= int(currency) * dividier
            dividier /= 100

        resp_body = {
            "gold": currencies[0],
            "silver": currencies[1],
            "copper": currencies[2],
            "items": resp_data["items"]
        }
        return resp_body
    return resp_data, resp.status_code



    return response_body, resp.status_code

@app.after_request
def after_request(response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    response.headers["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS, PUT, DELETE"
    response.headers["Access-Control-Allow-Headers"] = "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization"
    return response

if __name__ == "__main__":
    pprint(gw2.api.get_commerce(settings.GW2))
